import{ Hero } from './hero';

export const HEROES: Hero[] = [
    { power: 15, name: 'Dr. Nice',
    nationality: 'Russia',
    health: 3,
    speed: 1,
    difficulty: 1,
    role: ['Anchor', 'Covering Fire', 'Shield'],
    description: '',
    ability: `Tachanka’s new gadget, the Shumikha Launcher, is perfect for area denial and for covering unfavorable angles.
                Its heavy incendiary grenades can bounce multiple times, and their fuse timer only starts after the first bounce,
                giving them a lot of versatility.
                Tachanka is equipped with 10 grenades at the beginning of a match and the Launcher can hold up to 7.
                For maximum effectiveness, use several grenades together to cover a wider area and prevent your opponents from
                avoiding them. Once launched, you’ll only be able to reload one at a time.`,
 },
    { power: 34, name: 'Bombasto',
    nationality: 'Morocco',
    health: 3,
    speed: 1,
    difficulty: 2,
    description: `“Lead by example and the mountain will move.”
                    Jalal El Fassi was raised on stories of the Fortress, the legendary training facility in the Atlas Mountains.
                    Its incomparable reputation, along with its famed “Kaids” (Commander) were seared into El Fassi.
                    He joined the GIGR (Royal Moroccan Gendarmerie) at 18, demonstrating natural leadership skills under fire.
                    His ability to effectively command specialized squadrons in joint Trans-Saharan Counterterrorism Initiative (TSCTP)
                    operations led to his promotion as Captain. After years of service, the Fortress’ reigning Commander chose his successor:
                    El Fassi, whose authoritative but ethical style made him a worthy “Kaid”.

                    His imposing presence and strict values led to success with his graduates, and the Fortress quickly gained
                    accolades. He further proved its excellence through the thousands of soldiers who trained or taught on its
                    grounds. Over years of negotiating, he welcomed troops from across Africa and the Middle-East, fostering stronger
                    ties between TSCTP members and African Standby Force countries`,

    role: ['Anchor', 'Anti Hard Breach', 'Secure'],
    ability: `Kaid’s "Rtila" Electroclaw can electrify reinforced walls and hatches, barbed wire, and deployable shields, making it
                tougher for the enemy to breach the Kasbah and any facility he’s tasked with protecting. He enters the field with two
                of these throwable gadgets. Once they’ve attached themselves to a surface, they each electrify metallic objects
                within a 0.75 meter radius.`,
 },
    { power: 54, name: 'Celeritas',
    nationality: 'Brazil',
    health: 1,
    speed: 3,
    difficulty: 3,
    description: `“Not every child gets to choose who they grow up to be.”
                     Pereira is seventh of ten kids, born to a widowed mother in a small town of the state of São Paulo.
                     After moving to Rio de Janeiro at a young age, Pereira was arrested for robbery and offered a choice between
                     juvenile reformatory and working with the Polícia Militar as an informant. Years of undercover operations within
                     Brazil’s notorious gangs made her a master of interrogation techniques and surveillance, and Pereira is now
                     frequently called upon to conduct training exercises within the force. Her skills in confined environment
                     tactics, extraction, and extreme risk situations are exemplary.
                     It was Pereira’s actions during the Rio de Janeiro Security Crisis that caught the attention of Rainbow,
                     though multiple reports note that she is considered a dangerous, free-roaming operator. Outside of her
                     professional record, little is known of Pereira’s life, except that she enjoys learning different languages and
                     fighting in unofficial Jiu Jitsu tournaments.`,

    role: ['Intel Denier', 'Intel Gatherer', 'Roam'],
    ability: `"The “Luison” is wrapped in a rock-based mineral fiber to both insulate and soundproof the silencer. This weapon
                 allows Caveira to incapacitate her target discreetly and conduct an interrogation. The “Luison” is not only
                 formidable to neutralize your opponent while remaining undetected; it also ensures that no one gets away.`,
 },
    { power: 75, name: 'Magneta',
    nationality: 'Germany',
    health: 2,
    speed: 2,
    difficulty: 2,
    description: `“Birthdays. Proposals. These should be surprises. No one wants a grenade to the face.”
                     Raised by his uncle, a mechanic with the Bundespolizei (BPOL) Aviation Group, Streicher was around engines and
                     motors at an early age. While he demonstrated skills in mechanics, it didn’t hold the challenge that he was
                     looking for. Since he had an affinity for complex machinery, his uncle encouraged him to study aeronautical
                     engineering at university, but strict academic form was an uncomfortable fit for him. So while Streicher’s
                     grades were acceptable, he had a tendency toward restless antics. Following his hobby and passion in flying,
                     he spent considerable time around pilots and aircraft. Private corporations sought him out, but Streicher joined
                     BPOL-Aviation Group because it offered him unique challenges, the opportunity to fly and to serve his country.
                     Almost immediately, Streicher began designing defensive weapon systems for BPOL including a ground-based Active
                     Defense System (ADS) prototype. It was this prototype that caught the attention of GSG 9 and Rainbow.`,

    role: ['Roam', 'Secure'],
    ability: `Jager can help his team create a defensive stronghold before roaming the map to intercept flanking attackers.
                He is capable of destroying incoming projectiles due to his unique deployable gadget : the Active Defense System or
                ADS.`,

 },
    { power: 86, name: 'RubberMan',
    nationality: 'Russia',
    health: 2,
    speed: 2,
    difficulty: 1,
    role: ['Bakc Lines', 'Soft Breach', 'Covering Fire'],
    description: '',
    ability: `Glaz is a back-line marksman, capable of soft breaching and providing covering fire from a distance.
                Glaz is capable of ranged shooting through his unique scope ability: HDS Flipsight.`,
 },
    { power: 37, name: 'Dynama',
    nationality: 'Germany',
    health: 2,
    speed: 2,
    difficulty: 3,
    role: ['Anti Roam', 'Crowd Control', 'Front Line', 'Shield'],
    description: `“I don’t like photos of myself. The lighting is always wrong.”
                     Kötz is a graduate of Hermann-Böse-Gymnasium, an elite academy specializing in science and language studies.
                     Throughout his education Kötz excelled both academically and athletically. Among his many unique qualities,
                     Kötz is multilingual with the ability to grasp the nuance of a language quickly. His physically imposing frame
                     and skill with his weapon make him formidable in tight space, room-to-room deployment. It’s his tactical
                     experience that secures him as a solid Rainbow elite. These abilities combined with his specialized academic
                     background and good-natured personality make him highly sought after. He easily transitioned from a Schnelle
                     Kräfte soldier in Kosovo to a member of GSG 9. Kötz has been integral to fostering training practices and good
                     will between India’s National Security Guard and GSG 9.`,
    ability: `Ballistic shield.
                Three rows of eight flash-bang grenades provide a high intensity light source`,
 },
    { power: 58, name: 'Dr. IQ',
    nationality: 'Australia',
    health: 3,
    speed: 1,
    difficulty: 1,
    role: ['Anti Roam', 'Area Denial', 'Crowd Control'],
    description: `““You’ve gotta have a couple of roos loose in the top paddock to sign up for this life, but there’s nowhere I’d rather be.”

The eldest of five, Fairous has demonstrated leadership potential throughout her life. From an early age Fairous worked with engines and competed in robot championships. Her mechanic background was heavily influenced by her father, a military aircraft mechanic. Fairous joined the Australian army reserves to further develop her mechanical engineering skills and to have access to the best equipment. After graduating from university she chose Army branch of the Australian Defense Force, where she honed her mechanical prowess. Fairous also has expertise in a range of weapons, intelligence gathering and close protection duties, and has the ability to adapt to unexpected situations and remain calm in dangerous situations. This unique skill set made her an essential recruit into the SASR Mobility Platoon. Fairous was awarded the National Emergency Medal for her part in saving firefighters. She has been offered lead positions but has repeatedly turned them down.`,
    ability: `Trax Stingers' design is a portable version that when activated, deploys a hexagonal cluster of spikes, covering uneven ground better than a straight mat. Once deployed they can replicate and spread out over a large area. This feature, unique for an Attacker, enables them to use the Trax as an obstacle to reshape the map and control an area. Effects on the enemy include slowing them down and causing damage when stepped on. Trax Stingers will be destroyed when shot or dealt impact or thermal damage.`

 },
    { power: 89, name: 'Magma',
    nationality: 'South Korea',
    health: 1,
    speed: 3,
    difficulty: 3,
    role: ['Intel Denier', 'Roam'],
    description: `“Don’t worry about me. I was never here.”

Much of Hwa’s early records are unclear. His birth parents remain unknown. What is known is that he suffered the loss of his older brother and father in their bid to reach the Republic of Korea. School records track him from late adolescence, when he was adopted into the Hwa family. Initial reports indicate signs of early trauma – reclusive, slow reader – however, his later grades show great improvement and focus.

Hwa gravitated toward electronics and electrical engineering. Wanting to use his talents and demonstrate pride for his country, he chose to join the ROK Navy. He later earned a berth in the ROKN UDT/SEALs, with a knack for radar systems and stealth technology. Handpicked by the respected 707th Special Mission Battalion for unconventional warfare missions, he proved to be quick and lethal. Hwa served together with Specialist Grace “Dokkaebi” Nam until their recruitment into Rainbow.`,
    ability: `Chul Kyung’s quiet lethality in the field allows him to rely on his Electronic Rendering Cloak (ERC-7) to remain undetected. The ERC-7 uses Diminished Reality technology to remove perceivable stimuli from its direct environment. Vigil carries a prototype in his backpack, which scans surrounding electronic devices and wipes his image from any cameras in view.`,
 },
    { power: 20, name: 'Tornado',
    nationality: 'South Korea',
    health: 1,
    speed: 3,
    difficulty: 3,
    role: ['Intel Denier', 'Roam'],
    description: `“Don’t worry about me. I was never here.”

Much of Hwa’s early records are unclear. His birth parents remain unknown. What is known is that he suffered the loss of his older brother and father in their bid to reach the Republic of Korea. School records track him from late adolescence, when he was adopted into the Hwa family. Initial reports indicate signs of early trauma – reclusive, slow reader – however, his later grades show great improvement and focus.

Hwa gravitated toward electronics and electrical engineering. Wanting to use his talents and demonstrate pride for his country, he chose to join the ROK Navy. He later earned a berth in the ROKN UDT/SEALs, with a knack for radar systems and stealth technology. Handpicked by the respected 707th Special Mission Battalion for unconventional warfare missions, he proved to be quick and lethal. Hwa served together with Specialist Grace “Dokkaebi” Nam until their recruitment into Rainbow.`,
    ability: `Chul Kyung’s quiet lethality in the field allows him to rely on his Electronic Rendering Cloak (ERC-7) to remain undetected. The ERC-7 uses Diminished Reality technology to remove perceivable stimuli from its direct environment. Vigil carries a prototype in his backpack, which scans surrounding electronic devices and wipes his image from any cameras in view.`

 }
  ];