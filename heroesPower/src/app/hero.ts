export interface Hero {
    power: number;
    name: string;
    nationality: string,
    health: number,
    speed: number,
    difficulty: number,
    role: string[],
    description: string,
    ability: string
  }