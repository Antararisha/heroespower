import { HeroesDetailsComponent } from './../heroes-details/heroes-details.component';
import { Component, OnInit } from '@angular/core'
import { Hero } from '../hero';
import { HEROES } from '../mock-heroes';

@Component({
  selector: 'app-heroes-component',
  templateUrl: './heroes-component.component.html',
  styleUrls: ['./heroes-component.component.css']
})
export class HeroesComponentComponent implements OnInit {
  heroes = HEROES;
  selectedHero?: Hero;

  constructor() { }

  ngOnInit(): void {
  }
  heroList: Hero[] = this.sortHeroList(HEROES);
  sortHeroList(heroes: Hero[]): Hero[] {
    heroes.sort((a, b) => {
      return a.power - b.power;
    });
    return heroes;
  }


  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

}
