import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeroesComponentComponent } from './heroes-component/heroes-component.component';
import { HeroesDetailsComponent } from './heroes-details/heroes-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroesComponentComponent,
    HeroesDetailsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
