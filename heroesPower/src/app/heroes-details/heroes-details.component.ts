import { Component, OnInit, Input,Output, EventEmitter  } from '@angular/core';
import { Hero } from '../hero';

@Component({
  selector: 'app-heroes-details',
  templateUrl: './heroes-details.component.html',
  styleUrls: ['./heroes-details.component.css']
})
export class HeroesDetailsComponent implements OnInit {
  selectedHero?: Hero;
  @Input() hero?: Hero;
  constructor() { }
  
  ngOnInit(): void {
  }
  getBackgroundColor(power: number): string {
    if (power >= 80) {
      return '#a70000';
    } else if (60 <= power && power <= 79) {
      return '#ff0000';
    } else if (40 <= power && power <= 69) {
      return '#ff5252';
    } else if (20 <= power && power <= 59) {
      return '#ff7b7b';
    }
    return '#ffbaba';
  }
  


}



